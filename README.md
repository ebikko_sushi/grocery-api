# grocery-api

# Example .env

NODE_ENV = 'dev'
PORT = 3000
DEV_MONGO_URI = 'mongodb://localhost/grocery-db'
TEST_MONGO_URI = 'mongodb://localhost/grocery-db-test'
