const mongoose = require('mongoose');

const Product = require('../models/product');

// creates a single product
exports.createProduct = (req, res) => {
    const product = new Product({
        _id: mongoose.Types.ObjectId(),
        prod_name: req.body.prod_name, 
        brand_name: req.body.brand_name, 
        barcode: req.body.barcode, 
    });

    if (product.barcode.toString().length === 12) {
        product.save()
            .then(result => {
                res.status(201).json({
                    item: {
                        _id: result._id,
                        prod_name: result.prod_name,
                        brand_name: result.brand_name,
                        barcode: result.barcode,
                    },
                    success: true
                });
            })
            .catch(err => {
                res.status(500).json(err);
            });
    } else {
        res.status(400).json({
            items: [],
            message: "Barcode must conform with UPC12 standards",
            success: false
        })
    }
}

// reads all products
exports.readAllProducts = (req, res) => {
    Product.find()
        .select('-__v')
        .exec()
        .then(result => {
            res.status(200).json({
                items: result,
                success: true,
                count: result.length
            });
        })
        .catch(err => {
            res.status(500).json(err);
        });
}

// reads single product based on product id
exports.readProductById = (req, res) => {
    Product.findById(req.params.id)
        .select('-__v')
        .exec()
        .then(result => {
            if (result) {
                res.status(200).json({
                    items: [result],
                    success: true
                });
            } else {
                res.status(404).json({
                    items: [],
                    message: `No product found with id: ${req.params.id}`,
                    success: false
                });
            }
        })
        .catch(err => {
            res.status(500).json({
                err: err,
                success: false
            });
        });
}

// updates single product based on product id 
exports.updateProductById = (req, res) => {
    if (req.body.barcode.toString().length === 12) {
        Product.findOneAndUpdate({_id: req.params.id}, req.body)
            .exec()
            .then(result => {
                console.log('result'. result);
                res.status(200).json({
                    old_item: {
                        _id: result._id,
                        prod_name: result.prod_name,
                        brand_name: result.brand_name,
                        barcode: result.barcode,
                    },
                    success: true
                });
            })
            .catch(err => {
                if (Object.keys(err).length === 0) {
                    res.status(404).json({
                        err: err,
                        message: `No product found with id: ${req.params.id}`,
                        success: false
                    });
                } else {
                    res.status(500).json({
                        err: err,
                        success: false
                    });
                }
            });
    } else {
        res.status(400).json({
            items: [],
            message: "Barcode must conform with UPC12 standards",
            success: false
        })
    }
}

// deletes single product based on product id
exports.deleteProductById = (req, res) => {
    Product.findOneAndDelete({_id: req.params.id})
        .exec()
        .then(result => {
            res.status(200).json({
                item: {
                    _id: result._id,
                    prod_name: result.prod_name,
                    brand_name: result.brand_name,
                    barcode: result.barcode,
                },
                success: true
            });
        })
        .catch(err => {
            if (Object.keys(err).length === 0) {
                res.status(404).json({
                    err: err,
                    message: `No product found with id: ${req.params.id}`,
                    success: false
                });
            }
            
            res.status(500).json({
                err: err,
                success: false
            });
        });
}