const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    prod_name: {
        type: String,
        required: true
    },
    brand_name: {
        type: String,
        required: true,
    },
    barcode: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Product', productSchema);