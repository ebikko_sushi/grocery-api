const express = require('express');
const router = express.Router();

const productsController = require('../controllers/products');

router.get('/', productsController.readAllProducts);
router.get('/:id', productsController.readProductById);
router.post('/', productsController.createProduct);
router.put('/:id', productsController.updateProductById);
router.delete('/:id', productsController.deleteProductById);

module.exports = router;