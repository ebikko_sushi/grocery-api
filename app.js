require('dotenv').config();

const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');

const productsRoute = require('./api/routes/products');

let mongoUri;

if (process.env.NODE_ENV === 'dev') {
    mongoUri = process.env.DEV_MONGO_URI;
} else if (process.env.NODE_ENV === 'test') {
    mongoUri = process.env.TEST_MONGO_URI;
} else {
    console.error('NODE_ENV not specified, MongoDB URI could not be assigned');
}

mongoose.connect(mongoUri, 
    {
        useNewUrlParser: true,
        useFindAndModify: false
    }, 
    (err, res) => {
        if (err) {
            console.error('Error connecting to database', err);
        } else {
            console.log('Connected to database:', mongoUri);
        }
    }
);

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers', 
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        return res.status(200).json({});
    }

    next();
});

app.use('/products', productsRoute);

app.use((req, res, next) => {
    const err = new Error('Endpoint does not exist.');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500).json({
        error: {
            message: err.message
        }
    });
});

module.exports = app;